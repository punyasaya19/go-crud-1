package helper

import (
	"crypto/rand"
	"encoding/hex"

	"github.com/go-playground/validator/v10"
)

func GenerateRandomString(n int) (string, error) {
	b := make([]byte, n)
	if _, err := rand.Read(b); err != nil {
		return "", err
	}
	return hex.EncodeToString(b), nil
}

func GetFirstErrorValidation(errValidations []validator.FieldError) string {
	for _, validationError := range errValidations {
		return validationError.Field() + " is " + validationError.Tag()
	}
	return ""
}
