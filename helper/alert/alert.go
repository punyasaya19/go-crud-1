package alert

import "github.com/gin-gonic/gin"

func CreateAlertError(msg string, ctx *gin.Context) {
	// Simpan pesan error dalam cookie
	ctx.SetCookie("error", msg, 3600, "/", "", false, true)
}

func GetAlertError(ctx *gin.Context) string {
	// Baca cookie error
	errorMsg, err := ctx.Cookie("error")
	if err == nil {
		// Hapus cookie setelah membacanya
		ctx.SetCookie("error", "", -1, "/", "", false, true)
	}
	return errorMsg
}

func CreateAlertSuccess(msg string, ctx *gin.Context) {
	// Simpan pesan error dalam cookie
	ctx.SetCookie("success", msg, 3600, "/", "", false, true)
}

func GetAlertSuccess(ctx *gin.Context) string {
	// Baca cookie error
	errorMsg, err := ctx.Cookie("success")
	if err == nil {
		// Hapus cookie setelah membacanya
		ctx.SetCookie("success", "", -1, "/", "", false, true)
	}
	return errorMsg
}
