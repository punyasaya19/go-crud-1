package main

import (
	"go-miravalas-1/config"
	"go-miravalas-1/modules/auth"
	"go-miravalas-1/modules/dashboard"
	"go-miravalas-1/modules/user"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Auth login middleware
func MustLogin() gin.HandlerFunc {
	return func(c *gin.Context) {
		session, _ := config.Store.Get(c.Request, config.SESSION_ID)

		if len(session.Values) == 0 {
			// Belum login
			// Simpan pesan error dalam cookie
			c.SetCookie("error", "Login DULU!!!", 3600, "/", "", false, true)
			// Redirect ke halaman login
			c.Redirect(http.StatusFound, "/login")
			c.Abort()
			return
		} else {
			c.Next()
		}
	}
}

func main() {
	// Connect DB
	db := config.ConnectDB()

	// Init controller
	authController := auth.NewAuthController(db)
	userController := user.NewUserController(db)
	dashboardController := dashboard.NewDashboardController(db)

	// Routing
	router := gin.Default()

	// Render All views
	router.LoadHTMLGlob("views/**/*")

	// Static Route
	router.Static("/assets", "./assets")

	// Auth
	router.GET("/login", authController.Index)
	router.POST("/login", authController.Login)
	router.GET("/logout", authController.Logout)

	// Dashboard
	router.GET("/", MustLogin(), dashboardController.Index)

	// Users
	userRouter := router.Group("/user")
	userRouter.Use(MustLogin())
	{
		userRouter.GET("/", userController.Index)
		userRouter.GET("/create", userController.Create)
		userRouter.POST("/create", userController.Store)
		userRouter.GET("/edit/:id", userController.Edit)
		userRouter.POST("/edit/:id", userController.Update)
		userRouter.GET("/delete/:id", userController.Delete)
	}

	// Run Server
	router.Run(":3000")
}
