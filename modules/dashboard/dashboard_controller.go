package dashboard

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type DashboardController struct {
}

// Init controller
func NewDashboardController(con *gorm.DB) DashboardController {
	return DashboardController{}
}

// Functions
func (controller *DashboardController) Index(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "dashboard/index", nil)
}
