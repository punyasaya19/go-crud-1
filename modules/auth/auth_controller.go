package auth

import (
	"go-miravalas-1/helper/alert"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type AuthController struct {
	authService AuthService
}

// Init controller
func NewAuthController(con *gorm.DB) AuthController {
	return AuthController{authService: NewAuthService(con)}
}

// Functions
func (controller *AuthController) Index(ctx *gin.Context) {
	// // Baca cookie error
	// errorMsg, err := ctx.Cookie("error")
	// if err == nil {
	// 	// Hapus cookie setelah membacanya
	// 	ctx.SetCookie("error", "", -1, "/", "", false, true)
	// }
	errorMsg := alert.GetAlertError(ctx)
	ctx.HTML(http.StatusOK, "auth/login", gin.H{
		"Error": errorMsg,
	})
}

func (controller *AuthController) Login(ctx *gin.Context) {
	var authLoginInput AuthLoginInput

	err := ctx.ShouldBind(&authLoginInput)

	if err != nil {
		// // Simpan pesan error dalam cookie
		// ctx.SetCookie("error", "Gagal Parsing Data", 3600, "/", "", false, true)
		alert.CreateAlertError("Gagal Parsing Data", ctx)
		// Redirect ke halaman login
		ctx.Redirect(http.StatusFound, "/login")
		ctx.Abort()
		return
	}

	_, _, err = controller.authService.Login(authLoginInput, ctx)
	if err != nil {
		// // Simpan pesan error dalam cookie
		// ctx.SetCookie("error", err.Error(), 3600, "/", "", false, true)
		alert.CreateAlertError(err.Error(), ctx)
		// Redirect ke halaman login
		ctx.Redirect(http.StatusFound, "/login")
		ctx.Abort()
		return
	}

	// Jika berhasil redirect ke dashboard
	ctx.Redirect(http.StatusFound, "/")
}

func (controller *AuthController) Logout(ctx *gin.Context) {
	err := controller.authService.Logout(ctx)
	if err == nil {
		// Redirect ke halaman login
		ctx.Redirect(http.StatusFound, "/login")
		ctx.Abort()
		return
	}
}
