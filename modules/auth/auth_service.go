package auth

import (
	"errors"
	"go-miravalas-1/config"
	"go-miravalas-1/modules/user"
	"net/http"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type AuthServiceimpl struct {
	userRepository user.UserRepository
}

// Init service
func NewAuthService(con *gorm.DB) AuthService {
	return &AuthServiceimpl{userRepository: user.NewUserRepository(con)}
}

// Interface
type AuthService interface {
	Login(authLoginInput AuthLoginInput, ctx *gin.Context) (statusCode int, user user.User, err error)
	Logout(ctx *gin.Context) error
}

// Implement
func (service *AuthServiceimpl) Login(authLoginInput AuthLoginInput, ctx *gin.Context) (statusCode int, user user.User, err error) {
	// cek email ada atau tdk
	sts, user := service.userRepository.FindByEmail(authLoginInput.Email)
	if sts == 0 {
		// Jika data tdk ditemukan
		return http.StatusNotFound, user, errors.New("username tdk ditemukan")
	}

	// Cek password
	errPass := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(authLoginInput.Password))
	if errPass != nil {
		// Jika Password salah
		return http.StatusNotFound, user, errors.New("password Salah")
	}

	// Jika benar
	// Set session login
	session, _ := config.Store.Get(ctx.Request, config.SESSION_ID)
	session.Values["isLogin"] = true
	session.Values["id"] = user.ID
	session.Values["email"] = user.Email
	session.Values["name"] = user.Name
	session.Values["role"] = user.Role
	session.Save(ctx.Request, ctx.Writer)

	return http.StatusOK, user, nil
}

func (service *AuthServiceimpl) Logout(ctx *gin.Context) error {
	// Hapus session
	session, _ := config.Store.Get(ctx.Request, config.SESSION_ID)
	session.Options.MaxAge = -1
	session.Save(ctx.Request, ctx.Writer)

	return nil
}
