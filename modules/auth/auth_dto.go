package auth

type AuthLoginInput struct {
	Email    string `form:"email"`
	Password string `form:"password"`
}
