package branch

import "gorm.io/gorm"

type BranchRepositoryImp struct {
	db *gorm.DB
}

// Init Repo
func NewBranchRepository(con *gorm.DB) BranchRepository {
	return &BranchRepositoryImp{db: con}
}

// Interface
type BranchRepository interface {
	FindAll() []Branch
}

// Implement
func (repository *BranchRepositoryImp) FindAll() []Branch {
	var branches []Branch
	_ = repository.db.Find(&branches)
	return branches
}
