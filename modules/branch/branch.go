package branch

import (
	"time"

	"gorm.io/gorm"
)

type Branch struct {
	ID           int64  `form:"id" gorm:"column:id"`
	Code         string `form:"code" gorm:"column:code"`
	Name         string `form:"name" gorm:"column:name"`
	Phone        string `form:"phone" gorm:"column:phone"`
	Address      string `form:"address" gorm:"column:address"`
	Photo        string `form:"photo" gorm:"column:photo"`
	GmapsLink    string `form:"gmaps_link" gorm:"column:gmaps_link"`
	WorkingHour  string `form:"working_hour" gorm:"column:working_hour"`
	HutangCoaID  int64  `form:"hutang_coa_id" gorm:"column:hutang_coa_id"`
	PiutangCoaID int64  `form:"piutang_coa_id" gorm:"column:piutang_coa_id"`
	Latitude     string `form:"latitude" gorm:"column:latitude"`
	Longitude    string `form:"longitude" gorm:"column:longitude"`
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    gorm.DeletedAt
}
