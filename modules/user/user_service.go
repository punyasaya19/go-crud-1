package user

import (
	"errors"
	"go-miravalas-1/helper"
	"go-miravalas-1/modules/branch"
	"path/filepath"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type UserServiceImp struct {
	userRepository   UserRepository
	branchRepository branch.BranchRepository
}

// Init Service
func NewUserService(con *gorm.DB) UserService {
	return &UserServiceImp{
		userRepository:   NewUserRepository(con),
		branchRepository: branch.NewBranchRepository(con),
	}
}

// Interface
type UserService interface {
	GetAll() []User
	GetById(id int64) User
	GetCreateData() []branch.Branch
	Create(userCreateInput UserCreateInput, ctx *gin.Context) (User, error)
	Update(id int64, userEditInput UserEditInput, ctx *gin.Context) (User, error)
	Delete(id int64, ctx *gin.Context) (User, error)
}

// Implement
func (service *UserServiceImp) GetAll() []User {
	return service.userRepository.FindAll()
}

func (service *UserServiceImp) GetById(id int64) User {
	return service.userRepository.FindById(id)
}

func (service *UserServiceImp) GetCreateData() []branch.Branch {
	// Get all branch
	branches := service.branchRepository.FindAll()

	// Return
	return branches
}

func (service *UserServiceImp) Create(userCreateInput UserCreateInput, ctx *gin.Context) (User, error) {
	var user User

	// Hash Password
	passHashByte, err := bcrypt.GenerateFromPassword([]byte(userCreateInput.Password), 10)
	if err != nil {
		return user, errors.New("gagal hasing password")
	}
	userCreateInput.Password = string(passHashByte)

	// Cek jika role admin atau finance set branch id ke nil
	if userCreateInput.Role == "admin" {
		userCreateInput.BranchID = nil
	}

	// Upload file if exist
	var signatureFileName string
	if userCreateInput.Signature != nil && userCreateInput.Signature.Size > 0 {
		// Jika ada maka upload
		randomString, err := helper.GenerateRandomString(16)
		if err != nil {
			return user, errors.New("gagal generate random string for signature file")
		}
		fileExtension := filepath.Ext(userCreateInput.Signature.Filename)
		signatureFileName = randomString + fileExtension
		signatureFilePath := "assets/user/signature/" + signatureFileName

		// Upload
		err = ctx.SaveUploadedFile(userCreateInput.Signature, signatureFilePath)
		if err != nil {
			return user, errors.New("gagal Upload signature file = " + err.Error())
		}

		// Set signature user
		user.Signature = &signatureFileName
	}

	// Set variable user yang mau di create
	user.Name = userCreateInput.Name
	user.Email = userCreateInput.Email
	user.Password = userCreateInput.Password
	user.Role = userCreateInput.Role
	user.BranchID = userCreateInput.BranchID
	user.Phone = userCreateInput.Phone

	// Create User
	_, err = service.userRepository.Create(user)
	if err != nil {
		return user, err
	}

	// Return
	return user, nil
}

func (service *UserServiceImp) Update(id int64, userEditInput UserEditInput, ctx *gin.Context) (User, error) {
	var user User
	user.ID = id

	// Cek password ganti atau tdk
	if userEditInput.Password != "" {
		// Hash Password
		passHashByte, err := bcrypt.GenerateFromPassword([]byte(userEditInput.Password), 10)
		if err != nil {
			return user, errors.New("gagal hasing password")
		}
		userEditInput.Password = string(passHashByte)
		user.Password = userEditInput.Password
	}

	// Cek jika role admin atau finance set branch id ke nil
	if userEditInput.Role == "admin" {
		userEditInput.BranchID = nil
	}

	// Upload file if exist
	var signatureFileName string
	if userEditInput.Signature != nil && userEditInput.Signature.Size > 0 {
		// Jika ada maka upload
		randomString, err := helper.GenerateRandomString(16)
		if err != nil {
			return user, errors.New("gagal generate random string for signature file")
		}
		fileExtension := filepath.Ext(userEditInput.Signature.Filename)
		signatureFileName = randomString + fileExtension
		signatureFilePath := "assets/user/signature/" + signatureFileName

		// Upload
		err = ctx.SaveUploadedFile(userEditInput.Signature, signatureFilePath)
		if err != nil {
			return user, errors.New("gagal Upload signature file = " + err.Error())
		}

		// Set signature user
		user.Signature = &signatureFileName
	}

	// Set variable user yang mau di create
	user.Name = userEditInput.Name
	user.Email = userEditInput.Email
	user.Role = userEditInput.Role
	user.BranchID = userEditInput.BranchID
	user.Phone = userEditInput.Phone

	// Create User
	_, err := service.userRepository.Update(user)
	if err != nil {
		return user, err
	}

	// Return
	return user, nil
}

func (service *UserServiceImp) Delete(id int64, ctx *gin.Context) (User, error) {
	user := User{
		ID: id,
	}
	user, err := service.userRepository.Delete(user)
	if err != nil {
		return user, err
	}

	// Return
	return user, nil
}
