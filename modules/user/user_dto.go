package user

import "mime/multipart"

type UserCreateInput struct {
	Name      string                `form:"name" validate:"required"`
	Email     string                `form:"email" validate:"required"`
	Password  string                `form:"password" validate:"required"`
	Role      string                `form:"role" validate:"required"`
	BranchID  *int                  `form:"branch_id" validate:"required"`
	Signature *multipart.FileHeader `form:"signature"`
	Phone     string                `form:"phone" validate:"required"`
}

type UserEditInput struct {
	Name      string                `form:"name" validate:"required"`
	Email     string                `form:"email" validate:"required"`
	Password  string                `form:"password"`
	Role      string                `form:"role" validate:"required"`
	BranchID  *int                  `form:"branch_id" validate:"required"`
	Signature *multipart.FileHeader `form:"signature"`
	Phone     string                `form:"phone" validate:"required"`
}
