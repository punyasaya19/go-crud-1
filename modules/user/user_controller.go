package user

import (
	"fmt"
	"go-miravalas-1/helper"
	"go-miravalas-1/helper/alert"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

type UserController struct {
	userService UserService
}

// Init controller
func NewUserController(con *gorm.DB) UserController {
	return UserController{userService: NewUserService(con)}
}

// Functions
func (controller *UserController) Index(ctx *gin.Context) {
	users := controller.userService.GetAll()
	errMsg := alert.GetAlertError(ctx)
	successMsg := alert.GetAlertSuccess(ctx)

	ctx.HTML(http.StatusOK, "user/index", gin.H{
		"Title":   "User",
		"Users":   users,
		"Error":   errMsg,
		"Success": successMsg,
	})
}

func (controller *UserController) Create(ctx *gin.Context) {
	errMsg := alert.GetAlertError(ctx)
	successMsg := alert.GetAlertSuccess(ctx)
	branches := controller.userService.GetCreateData()

	ctx.HTML(http.StatusOK, "user/create", gin.H{
		"Title":    "Create User",
		"Error":    errMsg,
		"Success":  successMsg,
		"Branches": branches,
	})
}

func (controller *UserController) Store(ctx *gin.Context) {
	var userCreateInput UserCreateInput

	err := ctx.ShouldBind(&userCreateInput)
	if err != nil {
		alert.CreateAlertError("gagal parsing data", ctx)
		ctx.Redirect(http.StatusFound, "/user/create")
		ctx.Abort()
		return
	}

	// Validation
	validate := validator.New(validator.WithRequiredStructEnabled())
	err = validate.Struct(&userCreateInput)
	if err != nil {
		validationErrors := err.(validator.ValidationErrors)
		fmt.Println(helper.GetFirstErrorValidation(validationErrors))
		alert.CreateAlertError("Validasi error", ctx)
		ctx.Redirect(http.StatusFound, "/user/create")
		ctx.Abort()
		return
	}

	_, err = controller.userService.Create(userCreateInput, ctx)
	if err != nil {
		alert.CreateAlertError(err.Error(), ctx)
		ctx.Redirect(http.StatusFound, "/user/create")
		ctx.Abort()
		return
	}

	// Jika berhasil
	alert.CreateAlertSuccess("Success Create User", ctx)
	ctx.Redirect(http.StatusFound, "/user")
}

func (controller *UserController) Edit(ctx *gin.Context) {
	// Get request id
	reqId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		alert.CreateAlertError("error parsing id request", ctx)
		ctx.Redirect(http.StatusFound, "/user")
		ctx.Abort()
		return
	}

	errMsg := alert.GetAlertError(ctx)
	successMsg := alert.GetAlertSuccess(ctx)
	branches := controller.userService.GetCreateData()
	user := controller.userService.GetById(int64(reqId))

	ctx.HTML(http.StatusOK, "user/edit", gin.H{
		"Title":    "Edit User",
		"Error":    errMsg,
		"Success":  successMsg,
		"Branches": branches,
		"User":     user,
	})
}

func (controller *UserController) Update(ctx *gin.Context) {
	// Get request id
	reqId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		alert.CreateAlertError("error parsing id request", ctx)
		ctx.Redirect(http.StatusFound, "/user/edit/"+ctx.Param("id"))
		ctx.Abort()
		return
	}

	var userEditInput UserEditInput
	err = ctx.ShouldBind(&userEditInput)
	if err != nil {
		alert.CreateAlertError("gagal parsing data", ctx)
		ctx.Redirect(http.StatusFound, "/user/edit/"+ctx.Param("id"))
		ctx.Abort()
		return
	}

	_, err = controller.userService.Update(int64(reqId), userEditInput, ctx)
	if err != nil {
		alert.CreateAlertError(err.Error(), ctx)
		ctx.Redirect(http.StatusFound, "/user/edit/"+ctx.Param("id"))
		ctx.Abort()
		return
	}

	// Jika berhasil
	alert.CreateAlertSuccess("Success Update User", ctx)
	ctx.Redirect(http.StatusFound, "/user")
}

func (controller *UserController) Delete(ctx *gin.Context) {
	// Get request id
	reqId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		alert.CreateAlertError("error parsing id request", ctx)
		ctx.Redirect(http.StatusFound, "/user")
		ctx.Abort()
		return
	}

	_, err = controller.userService.Delete(int64(reqId), ctx)
	if err != nil {
		alert.CreateAlertError(err.Error(), ctx)
		ctx.Redirect(http.StatusFound, "/user")
		ctx.Abort()
		return
	}

	// Jika berhasil
	alert.CreateAlertSuccess("Success Delete User", ctx)
	ctx.Redirect(http.StatusFound, "/user")
}
