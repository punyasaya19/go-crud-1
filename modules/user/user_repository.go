package user

import (
	"gorm.io/gorm"
)

type UserRepositoryImp struct {
	db *gorm.DB
}

// Init Repo
func NewUserRepository(con *gorm.DB) UserRepository {
	return &UserRepositoryImp{db: con}
}

// Interface
type UserRepository interface {
	FindAll() []User
	FindById(id int64) User
	FindByEmail(email string) (sts int, usr User)
	Create(user User) (User, error)
	Update(user User) (User, error)
	Delete(user User) (User, error)
}

// Implement
func (repository *UserRepositoryImp) FindAll() []User {
	var users []User

	// repository.db.Model(&user).Association("Branch")
	// err := repository.db.Model(&user).Association("Branch")
	// if err != nil {
	// 	fmt.Println("Error Assosiation")
	// 	panic(err)
	// }

	repository.db.Preload("Branch").Find(&users)

	// _ = repository.db.Find(&users)

	return users
}

func (repository *UserRepositoryImp) FindById(id int64) User {
	var user User
	_ = repository.db.Preload("Branch").First(&user, id).Error
	// _ = repository.db.First(&user, id)
	return user
}

func (repository *UserRepositoryImp) FindByEmail(email string) (sts int, user User) {
	result := repository.db.Where("email = ?", email).Limit(1).Find(&user)
	if result.Error != nil {
		return 0, user
	}
	return int(result.RowsAffected), user
}

func (repository *UserRepositoryImp) Create(user User) (User, error) {
	result := repository.db.Create(&user)

	if result.Error != nil {
		return user, result.Error
	}

	return user, nil
}

func (repository *UserRepositoryImp) Update(user User) (User, error) {
	result := repository.db.Updates(&user)

	if result.Error != nil {
		return user, result.Error
	}

	return user, nil
}

func (repository *UserRepositoryImp) Delete(user User) (User, error) {
	result := repository.db.Delete(&user)

	if result.Error != nil {
		return user, result.Error
	}

	return user, nil
}
