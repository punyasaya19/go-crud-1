package user

import (
	"go-miravalas-1/modules/branch"
	"time"

	"gorm.io/gorm"
)

type User struct {
	ID          int64  `form:"id" gorm:"column:id"`
	Name        string `form:"name" gorm:"column:name"`
	Email       string `form:"email" gorm:"column:email"`
	Password    string `form:"password" gorm:"column:password"`
	Role        string `form:"role" gorm:"column:role"`
	BranchID    *int   `form:"branch_id" gorm:"column:branch_id"`
	Branch      branch.Branch
	Signature   *string `form:"signature" gorm:"column:signature"`
	PhonePrefix *string `form:"phone_prefix" gorm:"column:phone_prefix"`
	Phone       string  `form:"phone" gorm:"column:phone"`
	CreatedAt   *time.Time
	UpdatedAt   *time.Time
	DeletedAt   *gorm.DeletedAt
}
