package config

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// DB config
const DB_DRIVER = "mysql"
const DB_HOST = "localhost"
const DB_PORT = "3306"
const DB_USER = "root"
const DB_PASS = ""
const DB_NAME = "go_mitravalas_1"

func ConnectDB() *gorm.DB {
	dsn := DB_USER + ":" + DB_PASS + "@tcp(" + DB_HOST + ":" + DB_PORT + ")/" + DB_NAME + "?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	db = db.Debug()

	// Migration
	// err = db.AutoMigrate()
	// if err != nil {
	// 	panic(err)
	// }

	return db
}
